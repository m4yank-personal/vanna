export default class VannaCanvas {
    width:number;
    height:number;
    screen:string[][];

    // Constructor Setting up the empty canvas
    constructor(width:number, height:number){
        this.width = width;
        this.height = height;
        this.screen = [];
        this.reset();

    }

    // reset all screen value to space
    reset():void
    {
        for(let i: number = 0; i <= this.height; i++) {
            this.screen[i] = [];
            for(let j: number = 0; j<= this.width; j++) {
                this.screen[i][j] = ' ';
            }
        }
        this.printTheScreen();
    }

    // print value on pixel
    printTheScreen():void
    {
        process.stdout.write("\n");
        for( let i:number = 0; i<=this.width+1; i++)
        {
            process.stdout.write("-");
        }
        process.stdout.write("\n");
        for(let i:number = 0; i<=this.height; i++)
        {
            process.stdout.write("|");
            for(let j:number=0; j<=this.width; j++)
            {
                process.stdout.write(this.screen[i][j]);
            }
            process.stdout.write("|");
            process.stdout.write("\n");
        }
        for( let i:number = 0; i<=this.width+1; i++)
        {
            process.stdout.write("-");
        }
        process.stdout.write("\n");
    }

    // draw a Line
    drawLine(x0:number,y0:number,x1:number,y1:number):void
    {
        if(x0 === x1)
        {
            for (let y = y0; y <= y1; y++)
            {
                this.screen[y][x0]='x';
            }
        }
        else if (y0 === y1)
        {
            for (let x = x0; x<=x1; x++)
            {
                this.screen[y0][x] = 'x';
            }
        }
    }
    // Check if within bound 
    checkBounds(x0:number, y0:number): boolean
    {
        if(x0<=this.width && y0 <= this.height)
            return true;
        else
            return false;
    }
    // draw rectangle
    drawRectangle(x0:number,y0:number,x1:number,y1:number):void
    {
        const height:number = y1-y0;
        const width:number = x1-x0;
        this.drawLine(x0,y0, x0+width, y0);
        this.drawLine(x0,y0,x0,y0+height);
        this.drawLine(x0,y0+height,x0+width,y0+height);
        this.drawLine(x0+width,y0,x1,y1);
        this.printTheScreen();
    }

}