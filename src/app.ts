import VannaCanvas from './VannaCanvas';
import { createInterface } from "readline";

const readLine = createInterface({
    input: process.stdin,
    output: process.stdout
});

let oneCanvas:VannaCanvas;

// Check weather user really created a canvas or not
function checkCanvas(canvas:VannaCanvas): boolean
{
    if(typeof canvas !== 'undefined')
    {
        return true
    }
    else
    {
        process.stdout.write("Please create a canvas first \n");
        return false
    }
}

// For now system can accept only straight line forward
// or from top to bottom no diagonal lines or backward lines
function verifyIfValidLine(x1:number,y1:number, x2:number, y2:number):boolean
{
    if(x1 === x2 && y1 < y2 && oneCanvas.checkBounds(x1,y1) && oneCanvas.checkBounds(x2,y2))
    {
        return true;
    }
    else if(x1 < x2 && y1 === y2 && oneCanvas.checkBounds(x1,y1) && oneCanvas.checkBounds(x2,y2))
    {
        return true;
    }
    else
    {
        process.stdout.write("Please a valid line and within bound \n");
        return false;
    }
}
// For now system can accept only rectangles with x2 y2 value being bigger instead of printing a rectangle in reverse direction.
function verifyIfValidRectangle(x1:number,y1:number, x2:number, y2:number):boolean
{
    if(x1 < x2 && y1 < y2 && oneCanvas.checkBounds(x1,y1) && oneCanvas.checkBounds(x2,y2))
    {
        return true;
    }
    else
    {
        process.stdout.write("Pleae enter a valid rectangle for now and within bounds\n");
        return false;
    }
}

// Show all possible command a user guide may be?
function showPossibleCommands():void
{
    process.stdout.write("C w h where C would initiate creating canvas and w is width, h is height\n");
    process.stdout.write("L x1 y1 x2 y2 Should create a new line from (x1,y1) to (x2,y2). Currently only horizontal or vertical lines are supported. Horizontal and vertical lines will be drawn using the 'x' character.\n");
    process.stdout.write("R x1 y1 x2 y2 Should create a new rectangle, whose upper left corner is (x1,y1) and lower right corner is (x2,y2). Horizontal and vertical lines will be drawn using the 'x' character.\n");
    process.stdout.write("Q Should quit the program.\n");
}

// Start asking questions
function askQuestions(): void {
    readLine.question("Enter Command: ",(answer)=>{
        const arr:string[] = answer.split(" ",5);
        switch(arr[0])
        {
            case 'C':
                oneCanvas = new VannaCanvas(parseInt(arr[1],10),parseInt(arr[2],10));
                askQuestions();
            break;
            case 'R':
                if(checkCanvas(oneCanvas) && verifyIfValidRectangle(parseInt(arr[1],10),parseInt(arr[2],10),parseInt(arr[3],10),parseInt(arr[4],10)))
                {
                    oneCanvas.drawRectangle(parseInt(arr[1],10),parseInt(arr[2],10),parseInt(arr[3],10),parseInt(arr[4],10));
                }
                askQuestions();
            break;
            case 'L':
                if(checkCanvas(oneCanvas) && verifyIfValidLine(parseInt(arr[1],10),parseInt(arr[2],10),parseInt(arr[3],10),parseInt(arr[4],10)))
                {
                    oneCanvas.drawLine(parseInt(arr[1],10),parseInt(arr[2],10),parseInt(arr[3],10),parseInt(arr[4],10));
                    oneCanvas.printTheScreen();
                }
                askQuestions();
            break;
            case 'Q': readLine.close(); break;
            default:
            // User did not enter any predefined input and hence lets just let them know possible commands.
            showPossibleCommands();
            askQuestions();
        }
    });
}

// Initiate the process
showPossibleCommands();
askQuestions();